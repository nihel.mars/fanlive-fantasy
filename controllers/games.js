ModelGames     = require('../models/game.js');
let jwtUtils     = require('../services/jwt.utils');
let i18n         = require('../services/locale.json');
const request = require('request');
var parse = require('xml-parser');

console.log("fff")
module.exports = {
//get all Games
  getGames: function (req, res) {
    // Getting auth header
    let headerAuth = req.headers['authorization'];
    let login = jwtUtils.getUser(headerAuth);
    if (login) {
        // Check user existe
        ModelUsers.getUserByPhone(login, function (err, me) {
            if (me) {
                ModelGames.getGames(function (err, game) {
                    if (err) {
                        throw err;
                    }
                    return res.status(200).json(game);
                });
            } else {
                res.status(403).json({
                    'success': false,
                    'error': {
                        'message': i18n.WrongToken,
                        'code': 403
                    }
                });
            }
        });
    } else {
        res.status(403).json({
            'success': false,
            'error': {
                'message': i18n.WrongToken,

                'code': 403
            }
        });
    }
},
//api opta games

    getGameFromOpta: function (req, res,callback) {
        _EXTERNAL_URL = 'http://omo.akamai.opta.net/competition.php?competition=24&season_id=2018&feed_type=F1&user=fanlive&psw=sP0n5oRl1v3';
        request(_EXTERNAL_URL, { json: true }, (err, res, body) => {
            let games = [];
        var result = parse(body)
        result.root.children[0].children.forEach(function (MatchInfo) {
            match={};
            if (MatchInfo.name=="MatchData"){
                match.uID=MatchInfo.attributes.uID;
            }
            MatchInfo.children.forEach(function (TeamData) {
                if ((TeamData.name == "TeamData")&& (TeamData.attributes.Side=="Home")) {
                    match.HomeTeam=TeamData.attributes.TeamRef;
                    match.HomeTeamScore=TeamData.attributes.Score;
                }
                if ((TeamData.name == "TeamData")&& (TeamData.attributes.Side=="Away")) {
                    match.AwayTeam=TeamData.attributes.TeamRef;
                    match.AwayTeamScore=TeamData.attributes.Score;
                }
            })
            games.push(match);
        })
        games.forEach(function (play) {

            ModelGames.addgame(play, callback);
        })
    })
    }
}
