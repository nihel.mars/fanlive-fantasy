ModelPlayers = require('../models/player.js');
var parse = require('xml-parser');
const request = require('request');
let jwtUtils = require('../services/jwt.utils');
let i18n = require('../services/locale.json');

module.exports = {
  //get all players
  getPlayers: function (req, res) {
    // Getting auth header
    let headerAuth = req.headers['authorization'];
    let login = jwtUtils.getUser(headerAuth);
    if (login) {
      // Check user existe
      ModelUsers.getUserByPhone(login, function (err, me) {
        if (me) {
          ModelPlayers.getPlayers(function (err, player) {
            if (err) {
              throw err;
            }
            return res.status(200).json(player);
          });
        } else {
          res.status(403).json({
            'success': false,
            'error': {
              'message': i18n.WrongToken,
              'code': 403
            }
          });
        }
      });
    } else {
      res.status(403).json({
        'success': false,
        'error': {
          'message': i18n.WrongToken,

          'code': 403
        }
      });
    }
  },
  
  //api opta players
  getPlayerFromOpta: function (req, res, callback) {
    _EXTERNAL_URL = 'http://omo.akamai.opta.net/competition.php?competition=24&season_id=2018&feed_type=F40&user=fanlive&psw=sP0n5oRl1v3';
    request(_EXTERNAL_URL, { json: true }, (err, res, body) => {
      let players = [];
      var result = parse(body)
      result.root.children[0].children.forEach(function (team) {
        
        team.children.forEach(function (player) {
          let play = {};
          play.uID=player.attributes.uID
          play.loan=player.attributes.loan
          
           player.children.forEach(function (child) {
            if (child.name == "Name") {
              play.Name = child.content;}
            if (child.name == "Position") {
              play.Position = child.content;}
            if((child.attributes.Type != '') && (child.content != '')){
             if((child.attributes.Type != undefined) && (child.content != undefined)){
             if (child.name == "Stat") {
              if (child.attributes.Type == 'on_loan_from') {
                play.on_loan_from = child.content;
                 }
             if (child.attributes.Type == 'first_name') {
                play.first_name = child.content; }
                if (child.attributes.Type == 'last_name'){
                  play.last_name = child.content; }
             if (child.attributes.Type == 'birth_date') {
                play.birth_date = child.content; }
              if (child.attributes.Type == 'birth_place') {
                play.birth_place = child.content; }
             if (child.attributes.Type == 'first_nationality'){
                play.first_nationality = child.content; }
              if (child.attributes.Type == 'preferred_foot') {
                play.preferred_foot = child.content; }  
              if (child.attributes.Type == 'weight') {
                play.weight = child.content; }
              if (child.attributes.Type == 'height') {
                play.height = child.content; }
                if (child.attributes.Type == 'jersey_num') {
                  play.jersey_num = child.content; }
                if (child.attributes.Type == 'real_position') {
                  play.real_position = child.content; }
                  if (child.attributes.Type == 'real_position_side') {
                    play.real_position_side = child.content; }
                    if (child.attributes.Type == 'join_date') {
                      play.join_date = child.content; }
                      if (child.attributes.Type == 'country') {
                        play.country = child.content; }
            } 
       // console.log(play)
            players.push(play); 
            
          }}
           
          }) 

         
        })
      })
      // console.log(players)
     players.forEach(function (player) {
        ModelPlayers.addplayer(player,callback);
      })

    })

  }
}

