ModelRefree = require('../models/refree.js');
var parse = require('xml-parser');
const request = require('request');
let jwtUtils = require('../services/jwt.utils');
let i18n = require('../services/locale.json');

module.exports = {
    //api opta refrees
    getRefreeFromOpta: function (req, res, callback) {
        _EXTERNAL_URL = 'http://omo.akamai.opta.net/competition.php?competition=24&season_id=2018&feed_type=F1&user=fanlive&psw=sP0n5oRl1v3';
        request(_EXTERNAL_URL, { json: true }, (err, res, body) => {
            let arbitres = [];
            var result = parse(body)
            result.root.children[0].children.forEach(function (match) {
                match.children.forEach(function (refree) {
                    refree.children.forEach(function (R) {
                        Ref = {};
                        
                        if (R.name == 'MatchOfficial') {
                            
                            Ref.uID = R.attributes.uID;
                            Ref.FirstName = R.attributes.FirstName
                            Ref.LastName = R.attributes.LastName
                            Ref.Type = R.attributes.Type 
                         }
                            arbitres.push(Ref); 
                    })
                  
                })
    
            })
            arbitres.forEach(function (arbitre) {
                ModelRefree.create(arbitre,callback);
              })
        })
    }
}