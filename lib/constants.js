let constants = {
    base_url:         "https://api.sandbox.stickbythem.com",  
    path_user_image : "https://api.sandbox.stickbythem.com/public/users/images/",
};
module.exports = Object.freeze(constants);