var mongoose = require('mongoose');
var Schema = mongoose.Schema;

teamSchema = require('../schemas/teamSchema.js');
var Team = module.exports = mongoose.model('Team', teamSchema)

//get all teams
module.exports.getTeam = function (callback, limit) {
  Team.find(callback).limit(limit);
}
module.exports.addteam = function (team, callback) {
  Team.create(team, callback);
}
//api opta team
module.exports.getTeamFromOpta = function (result) {
  let teams = [];
 request(_EXTERNAL_URL, { json: true }, (err, res, body) => {
     var result = parse(body)
    result.root.children[0].children.forEach(function (team) {
 
   teams.push({
    city: team.attributes.city, country: team.attributes.country, country_id: team.attributes.country_id,
    country_iso: team.attributes.country_iso, region_id: team.attributes.region_id, region_name: team.attributes.region_name
    , short_club_name: team.attributes.short_club_name, uID: team.attributes.uID, web_address: team.attributes.web_address
     });
       
   })
 
    return teams;
 
})
}
module.exports.updateStadiumTeam = function (team, data, callback) {
  Team.updateOne({ team: team }, { $set: { stadiums: data } }, {new: true} , callback);
}

