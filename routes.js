let express                 = require('express');
let usersController         = require('./controllers/users');
let palyersController       = require('./controllers/players');
let teamsController         = require('./controllers/teams');
let gamesController         = require('./controllers/games');
let stadiumsController      = require('./controllers/stadiums');
let refreesController       = require('./controllers/refrees');
let matchdaysController     = require('./controllers/matchdays');
let path                    = require('path');
let crypto                  = require('crypto');
let multer                  = require('multer');

// Upload user Image
let storageImage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, 'public/users/images/')
    },
    filename: function(req, file, cb) {
      crypto.pseudoRandomBytes(16, function (err, raw) {
        if (err) return cb(err)  
        cb(null, raw.toString('hex') + path.extname(file.originalname))
      });
    }
  });
  //filter image
const imageFilter = (req, file, cb) => {
  let type      = file.mimetype;
  let typeArray = type.split("/");
  if (typeArray[0] == "image") {
    cb(null, true);
  } else {
    cb('Invalid File Extension',false)
  }
}

  // Upload
let uploadImageUser  = multer({ storage: storageImage,limits: {fileSize: 65536},fileFilter: imageFilter }).single('image');
  //All route
exports.router =(function() {
    let apiRouter = express.Router(); 
    
    //users
    //Get all user
    apiRouter.route('/users').get(usersController.getAllUsers);
    //add user by phone number, if exist update code user
    apiRouter.route('/users/step1/addUser').post(usersController.step1);
    //Check code (body : code and registrationToken)
    apiRouter.route('/users/step2/addUser').put(usersController.step2);
     //Update username user 
     apiRouter.route('/users/step3/addUser/').put(usersController.step3);
     //Update picture for user 
     apiRouter.put('/users/step4/addUser/',  function(req, res) {
      uploadImageUser(req,res,function(err){
        usersController.step4(req,res,err);}); });
    //add teamname pseudo
    apiRouter.route('/users/step6/addUser/').put(usersController.step6);
    //add team to user
    apiRouter.route('/users/step5/addteamtoUser').put(usersController.step5);
    //notification firebase
    apiRouter.route('/users/send_Notif_To_All/').put(usersController.step7_AllToken);
     
    //teams
      //get all teams
      apiRouter.route('/teams').get(teamsController.getTeam);
      //team api
      apiRouter.route('/getTeamFromOpta').put(teamsController.getTeamFromOpta);
    
      //players
      //player api
      apiRouter.route('/getPlayerFromOpta').put(palyersController.getPlayerFromOpta);
      //get all players
      apiRouter.route('/Players').get(palyersController.getPlayers);

      //games 
      //games api 
      apiRouter.route('/getGameFromOpta').put(gamesController.getGameFromOpta);
      
      //stadiums 
      //stadiums api
      apiRouter.route('/getStadiumFromOpta').put(stadiumsController.getStadiumFromOpta);
      //get all stadiums
      apiRouter.route('/Stadiums').get(stadiumsController.getStadiums);
 
      //refrees 
      //refrees api
      apiRouter.route('/getRefreeFromOpta').put(refreesController.getRefreeFromOpta);

    //matchday
    apiRouter.route('/getMatchDayFromOpta').put(matchdaysController.getMatchDayFromOpta);

    return apiRouter;
})();