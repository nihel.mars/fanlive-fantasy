var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//team schema
module.exports = gameSchema = new Schema({
        uID:{ type: String,index: { unique: true }},
        HomeTeam:{type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
        HomeTeamScore :String,
        AwayTeam:{type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
        AwayTeamScore :String,
        DateGame :{ type: Date}
},
{
  timestamps: true
});
