var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//matchday schema
module.exports = matchdaySchema = new Schema({
    journee:{type: Number},
    date_debutJ:{type: String},
    date_finJ:{type: String},
},
{
  timestamps: true
});
