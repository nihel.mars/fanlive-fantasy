var mongoose = require('mongoose');
var Schema = mongoose.Schema;

//user shema
module.exports = userSchema = new Schema({
    teamname:{ type: String, index: { unique: true }},
    phone:{ type: String,  required:true, index: { unique: true }},
    region_code : { type: String,default: null },
    username: { type: String, default: "",index: { unique: true }},
    code: {type: String},
    active:{type: Boolean, default: false},
    path_img: { type: String, default: "" },
    registrationToken:String,
    Myteam:{ type: String, index: { unique: true }},
    favTeam:{type: mongoose.Schema.Types.ObjectId, ref: 'Team'},
    
},
{
    timestamps: true
});
